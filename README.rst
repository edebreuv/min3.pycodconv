==========================================================
Min3.PyCodConv: PEP8-uncompliant Python Coding Conventions
==========================================================

.. sectnum::
   :prefix: [
   :suffix: ]-



CASE CONVENTIONS
================

The definitions of the terms used in the "Convention" column of the table below can be found at: `letter case-separated words <https://en.wikipedia.org/wiki/Naming_convention_(programming)#Letter_case-separated_words>`_.

.. raw:: html

   <hr width=75% text-align=middle height=10px>

+---------------------+-------------------------+----------------------+
| Identifier          | Convention              | Example              |
+=====================+=========================+======================+
| Module import alias | 4 lowercase characters, | ``nmpy`` for Numpy   |
|                     | mostly consonants       |                      |
+---------------------+-------------------------+----------------------+
| Constant            | SCREAMING_SNAKE_CASE    | ``CONSTANT_EXAMPLE`` |
+---------------------+-------------------------+----------------------+
| Type                | snake_case              | ``class_example_t``  |
|                     | with a "_t" postfix     |                      |
+---------------------+-------------------------+----------------------+
| Type Hints          | snake_case              | ``type_hint_h``      |
|                     | with a "_h" postfix     |                      |
+---------------------+-------------------------+----------------------+
| Variable/Attribute  | snake_case              | ``instance_example`` |
+---------------------+-------------------------+----------------------+
| Function/Method     | UpperCamelCase          | ``FunctionExample``  |
+---------------------+-------------------------+----------------------+



NAMING CONVENTIONS
==================

General
-------

- **No identifier should be less than 3-characters long** (hence the ``Min3`` in the convention-set name).
- **Identifiers of private resources should start with an underscore** (as usual in Python).

See `examples <naming-conventions-examples-general_>`_.



Import
------

- **Constants and types should be imported "as is" with ``from ... import ...`` statements.**

    - When importing a class to be used as a parent class, an alias with a ``base_`` prefix can be defined to allow the child class to have the same identifier as its parent. If there is no ambiguity, ``base_t`` is simply used.

- **Functions in other modules should be accessed through a module alias.**
- **Imports should be sorted** with `isort <https://github.com/timothycrosley/isort>`_... *your imports, so you don't have to*.

- **One-letter aliases:**

    - a: ``ast``
    - c: ``collections.abc`` (Collections)
    - d: ``datetime``
    - e: ``inspect`` (inspEct, or Explore)
    - h: ``typing`` (type Hints)
    - i: ``itertools``
    - l: ``logging``
    - m: ``multiprocessing``
    - n: ``numpy``
    - o: ``os``
    - p: ``pathlib``
    - r: ``re``
    - s: ``sys``
    - t: ``types``

See `examples <naming-conventions-examples-import_>`_.



Variable/Attribute
------------------

- **The generic indexing loop variable should be** ``idx`` (as opposed to the traditional ``for i in ...``).
- **Variables in comprehensions and parameters of** ``lambda`` **functions should be prefixed with an underscore** since they are regarded as private elements. Moreover, for brevity, after the underscore, they **should be composed of 3 lowercase characters, most being consonants**.

See `examples <naming-conventions-examples-variable_>`_.



Function/Method
---------------

- **Functions/methods returning (an) object(s) should be composed of nouns and adjectives** (which includes verb in simple past form and verb+\ *ing*) and follow the rules

+--------------------------------+------------------------------+----------------------------+-------------------------------------+
| When returned object(s)...     | Name Composition             | Name Decoration            | Example                             |
+================================+==============================+============================+=====================================+
| is/are built from              |                              | "New[..(1)..]With..."      | Function: ``NewCircleWithRadius``;  |
| elementary parameters          |                              |                            | Class method: ``NewWithRadius``     |
+--------------------------------+                              +----------------------------+-------------------------------------+
| is/are built from              | Starts with a noun (1)       | "New[..(1)..]From..."      | Function: ``NewEllipseFromCircle``; |
| another object                 |                              |                            | Class method: ``NewFromCircle``     |
+--------------------------------+                              +----------------------------+-------------------------------------+
| result(s) from a conversion    |                              | "[..(1)..]AsUnrelatedType" | Function: ``CircleAsDict``;         |
| to an unrelated type           |                              |                            | Instance method: ``AsDict``         |
+--------------------------------+------------------------------+----------------------------+-------------------------------------+
| is/are (a) modified version(s) | Starts with an adjective (2) | "..(2)..[...]"             | Function: ``SortedAreas``;          |
| of itself/themselves           |                              |                            | Instance method: ``Sorted``         |
+--------------------------------+------------------------------+----------------------------+-------------------------------------+

- **Functions/methods returning nothing** [#nothing]_ **should start with a verb in imperative form** since they perform (an) action(s).
- **Nested functions should be prefixed with an underscore** since they are regarded as private elements.

.. [#nothing] i.e. ``None`` in Python's terms.

See `examples <naming-conventions-examples-function_>`_.



CODING HABITS
=============

Naming
------

In some circumstances, using a *real* name for an object does not improve readability (arguably). Instead, a *placeholder-style* naming is used, allowing to easily identify where and how the object is built, and leaving a proper name free to use as a temporary object of the same type (see examples below).



Type
----

- **The __init__ method should be minimal**, typically setting all attributes to None.
- **Instantiation should be done through class methods**.

See `examples <coding-habits-examples-type_>`_.



Function/Method
---------------

See `the typical skeleton of an instance method returning an object <coding-habits-examples-function_>`_.



CODE FORMATTING
===============

**The code should be formatted** with `Black <https://github.com/psf/black>`_, *The Uncompromising Code Formatter*.



EXCEPTIONS
==========

Type
----

Classes with certain inheritances should use a specific postfix instead of "_t":

+---------------------+---------+
| Parent class        | Postfix |
+=====================+=========+
| ``abc.ABC``         | _a      |
+---------------------+---------+
| ``enum.Enum``       | _e      |
+---------------------+---------+
| ``typing.Protocol`` | _p      |
+---------------------+---------+



Variable/Attribute
------------------

- Attributes of classes deriving from ``enum.Enum`` should use the SCREAMING_SNAKE_CASE convention.



NAMING CONVENTIONS - Examples
=============================

.. _naming-conventions-examples-general:

General
-------

Write

.. code:: python

    for row in range(a_numpy_ndarray.shape[0]):
        for col in range(a_numpy_ndarray.shape[1]):
            ...

instead of using ``for i`` and ``for j``.

Example of a private constant: ``_A_PRIVATE_CONSTANT``.



.. _naming-conventions-examples-import:

Import
------

.. code:: python

    from module_example import CONSTANT_EXAMPLE, class_example_t

.. code:: python

    from module_example import class_example_t as base_class_example_t
    ...
    class class_example_t(base_class_example_t):
        ...

.. code:: python

    import matplotlib.pyplot as pypl
    import numpy as nmpy
    import sys as sstm

Config file ``.isort.cfg`` for ``isort``:

.. code:: ini

    [settings]
    profile = black
    multi_line_output = 3



.. _naming-conventions-examples-variable:

Variable/Attribute
------------------

Variables in comprehensions and parameters of ``lambda`` functions:

.. code:: python

    perimeters = (2.0 * PI * _crl.radius for _crl in circles)
    large_perimeters = tuple(filter(lambda _prm: _prm > threshold, perimeters))



.. _naming-conventions-examples-function:

Function/Method
---------------

Examples of functions/methods returning nothing: ``PlotCircleInAxes`` for a function, ``PlotInAxes`` for an instance method.

The purpose of all the rules on functions/methods is to allow to read the code more *literally*. For example

.. code:: python

    circle = NewCircleWithRadius(10)
    print(circle.AsDict())



CODING HABITS - Examples
========================

.. _coding-habits-examples-type:

Type
----

Typical skeleton of an instantiation class method:

.. code:: python

    from __future__ import annotations
    ...
    class circle_t:
        ...
        @classmethod
        def NewWithRadius(cls, radius: int, /) -> circle_t:
            """"""
            instance = cls(...)
            ...
            return instance

Note the ``instance``, *placeholder-style* naming.



.. _coding-habits-examples-function:

Function/Method
---------------

Functions and instance methods returning an object should follow the skeleton

.. code:: python

    def NewWhatNotWith...(...) -> what_not_t:
        """"""
        output = ... # Some default what-not value
        ...
        return output

Note the ``output``, *placeholder-style* naming. When returning several objects, numbered outputs could be used if not impairing clarity: ``output_1``, ``output_2``...



REFERENCES
==========

Postfixes
---------

+---------+------------------------------------+
| Postfix | Usage                              |
+=========+====================================+
| _a      | Child class of  ``abc.ABC``        |
+---------+------------------------------------+
| _e      | Child class of ``enum.Enum``       |
+---------+------------------------------------+
| _h      | Type hint                          |
+---------+------------------------------------+
| _p      | Child class of ``typing.Protocol`` |
+---------+------------------------------------+
| _t      | Type                               |
+---------+------------------------------------+
